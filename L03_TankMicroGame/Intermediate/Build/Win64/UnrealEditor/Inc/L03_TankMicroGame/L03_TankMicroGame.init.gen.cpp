// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeL03_TankMicroGame_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_L03_TankMicroGame;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_L03_TankMicroGame()
	{
		if (!Z_Registration_Info_UPackage__Script_L03_TankMicroGame.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/L03_TankMicroGame",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xCBCD9312,
				0x59C959C2,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_L03_TankMicroGame.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_L03_TankMicroGame.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_L03_TankMicroGame(Z_Construct_UPackage__Script_L03_TankMicroGame, TEXT("/Script/L03_TankMicroGame"), Z_Registration_Info_UPackage__Script_L03_TankMicroGame, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xCBCD9312, 0x59C959C2));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
