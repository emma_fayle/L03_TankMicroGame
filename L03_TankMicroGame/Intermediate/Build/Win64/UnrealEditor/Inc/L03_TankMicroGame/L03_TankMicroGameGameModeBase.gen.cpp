// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "L03_TankMicroGame/L03_TankMicroGameGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeL03_TankMicroGameGameModeBase() {}
// Cross Module References
	L03_TANKMICROGAME_API UClass* Z_Construct_UClass_AL03_TankMicroGameGameModeBase_NoRegister();
	L03_TANKMICROGAME_API UClass* Z_Construct_UClass_AL03_TankMicroGameGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_L03_TankMicroGame();
// End Cross Module References
	void AL03_TankMicroGameGameModeBase::StaticRegisterNativesAL03_TankMicroGameGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AL03_TankMicroGameGameModeBase);
	UClass* Z_Construct_UClass_AL03_TankMicroGameGameModeBase_NoRegister()
	{
		return AL03_TankMicroGameGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_L03_TankMicroGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "L03_TankMicroGameGameModeBase.h" },
		{ "ModuleRelativePath", "L03_TankMicroGameGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AL03_TankMicroGameGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::ClassParams = {
		&AL03_TankMicroGameGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AL03_TankMicroGameGameModeBase()
	{
		if (!Z_Registration_Info_UClass_AL03_TankMicroGameGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AL03_TankMicroGameGameModeBase.OuterSingleton, Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AL03_TankMicroGameGameModeBase.OuterSingleton;
	}
	template<> L03_TANKMICROGAME_API UClass* StaticClass<AL03_TankMicroGameGameModeBase>()
	{
		return AL03_TankMicroGameGameModeBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AL03_TankMicroGameGameModeBase);
	struct Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AL03_TankMicroGameGameModeBase, AL03_TankMicroGameGameModeBase::StaticClass, TEXT("AL03_TankMicroGameGameModeBase"), &Z_Registration_Info_UClass_AL03_TankMicroGameGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AL03_TankMicroGameGameModeBase), 3269761706U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_1060910130(TEXT("/Script/L03_TankMicroGame"),
		Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
