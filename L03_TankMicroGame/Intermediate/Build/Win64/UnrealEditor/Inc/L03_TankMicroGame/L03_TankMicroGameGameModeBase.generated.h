// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef L03_TANKMICROGAME_L03_TankMicroGameGameModeBase_generated_h
#error "L03_TankMicroGameGameModeBase.generated.h already included, missing '#pragma once' in L03_TankMicroGameGameModeBase.h"
#endif
#define L03_TANKMICROGAME_L03_TankMicroGameGameModeBase_generated_h

#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_SPARSE_DATA
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_RPC_WRAPPERS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAL03_TankMicroGameGameModeBase(); \
	friend struct Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AL03_TankMicroGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(AL03_TankMicroGameGameModeBase)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAL03_TankMicroGameGameModeBase(); \
	friend struct Z_Construct_UClass_AL03_TankMicroGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AL03_TankMicroGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(AL03_TankMicroGameGameModeBase)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AL03_TankMicroGameGameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AL03_TankMicroGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AL03_TankMicroGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AL03_TankMicroGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AL03_TankMicroGameGameModeBase(AL03_TankMicroGameGameModeBase&&); \
	NO_API AL03_TankMicroGameGameModeBase(const AL03_TankMicroGameGameModeBase&); \
public:


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AL03_TankMicroGameGameModeBase(AL03_TankMicroGameGameModeBase&&); \
	NO_API AL03_TankMicroGameGameModeBase(const AL03_TankMicroGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AL03_TankMicroGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AL03_TankMicroGameGameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AL03_TankMicroGameGameModeBase)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_13_PROLOG
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_RPC_WRAPPERS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_INCLASS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_INCLASS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> L03_TANKMICROGAME_API UClass* StaticClass<class AL03_TankMicroGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_L03_TankMicroGame_Source_L03_TankMicroGame_L03_TankMicroGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
