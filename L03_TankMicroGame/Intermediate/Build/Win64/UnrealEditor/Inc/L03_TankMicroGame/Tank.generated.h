// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef L03_TANKMICROGAME_Tank_generated_h
#error "Tank.generated.h already included, missing '#pragma once' in Tank.h"
#endif
#define L03_TANKMICROGAME_Tank_generated_h

#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_SPARSE_DATA
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_RPC_WRAPPERS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATank(); \
	friend struct Z_Construct_UClass_ATank_Statics; \
public: \
	DECLARE_CLASS(ATank, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATank)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATank(); \
	friend struct Z_Construct_UClass_ATank_Statics; \
public: \
	DECLARE_CLASS(ATank, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATank)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATank(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATank) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATank); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATank); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATank(ATank&&); \
	NO_API ATank(const ATank&); \
public:


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATank(ATank&&); \
	NO_API ATank(const ATank&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATank); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATank); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATank)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_12_PROLOG
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_RPC_WRAPPERS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_INCLASS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_INCLASS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> L03_TANKMICROGAME_API UClass* StaticClass<class ATank>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_L03_TankMicroGame_Source_L03_TankMicroGame_Tank_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
