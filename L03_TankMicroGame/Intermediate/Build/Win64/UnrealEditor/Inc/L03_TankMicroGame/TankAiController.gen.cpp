// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "L03_TankMicroGame/TankAiController.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTankAiController() {}
// Cross Module References
	L03_TANKMICROGAME_API UClass* Z_Construct_UClass_ATankAiController_NoRegister();
	L03_TANKMICROGAME_API UClass* Z_Construct_UClass_ATankAiController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_L03_TankMicroGame();
	L03_TANKMICROGAME_API UClass* Z_Construct_UClass_ATank_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ATankAiController::StaticRegisterNativesATankAiController()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATankAiController);
	UClass* Z_Construct_UClass_ATankAiController_NoRegister()
	{
		return ATankAiController::StaticClass();
	}
	struct Z_Construct_UClass_ATankAiController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PossessedTank_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PossessedTank;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_StartingLocation_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_StartingLocation;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GoalLocation_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_GoalLocation;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATankAiController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_L03_TankMicroGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankAiController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Transformation" },
		{ "IncludePath", "TankAiController.h" },
		{ "ModuleRelativePath", "TankAiController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankAiController_Statics::NewProp_PossessedTank_MetaData[] = {
		{ "Category", "TankAiController" },
		{ "ModuleRelativePath", "TankAiController.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATankAiController_Statics::NewProp_PossessedTank = { "PossessedTank", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATankAiController, PossessedTank), Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATankAiController_Statics::NewProp_PossessedTank_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATankAiController_Statics::NewProp_PossessedTank_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankAiController_Statics::NewProp_StartingLocation_MetaData[] = {
		{ "Category", "TankAiController" },
		{ "ModuleRelativePath", "TankAiController.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATankAiController_Statics::NewProp_StartingLocation = { "StartingLocation", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATankAiController, StartingLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ATankAiController_Statics::NewProp_StartingLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATankAiController_Statics::NewProp_StartingLocation_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankAiController_Statics::NewProp_GoalLocation_MetaData[] = {
		{ "Category", "TankAiController" },
		{ "ModuleRelativePath", "TankAiController.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATankAiController_Statics::NewProp_GoalLocation = { "GoalLocation", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATankAiController, GoalLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ATankAiController_Statics::NewProp_GoalLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATankAiController_Statics::NewProp_GoalLocation_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATankAiController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATankAiController_Statics::NewProp_PossessedTank,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATankAiController_Statics::NewProp_StartingLocation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATankAiController_Statics::NewProp_GoalLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATankAiController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATankAiController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATankAiController_Statics::ClassParams = {
		&ATankAiController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATankAiController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATankAiController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATankAiController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATankAiController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATankAiController()
	{
		if (!Z_Registration_Info_UClass_ATankAiController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATankAiController.OuterSingleton, Z_Construct_UClass_ATankAiController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATankAiController.OuterSingleton;
	}
	template<> L03_TANKMICROGAME_API UClass* StaticClass<ATankAiController>()
	{
		return ATankAiController::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATankAiController);
	struct Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATankAiController, ATankAiController::StaticClass, TEXT("ATankAiController"), &Z_Registration_Info_UClass_ATankAiController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATankAiController), 115414592U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_3956043597(TEXT("/Script/L03_TankMicroGame"),
		Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
