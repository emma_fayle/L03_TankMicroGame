// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef L03_TANKMICROGAME_TankAiController_generated_h
#error "TankAiController.generated.h already included, missing '#pragma once' in TankAiController.h"
#endif
#define L03_TANKMICROGAME_TankAiController_generated_h

#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_SPARSE_DATA
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_RPC_WRAPPERS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATankAiController(); \
	friend struct Z_Construct_UClass_ATankAiController_Statics; \
public: \
	DECLARE_CLASS(ATankAiController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATankAiController)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATankAiController(); \
	friend struct Z_Construct_UClass_ATankAiController_Statics; \
public: \
	DECLARE_CLASS(ATankAiController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATankAiController)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankAiController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankAiController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankAiController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankAiController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankAiController(ATankAiController&&); \
	NO_API ATankAiController(const ATankAiController&); \
public:


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankAiController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankAiController(ATankAiController&&); \
	NO_API ATankAiController(const ATankAiController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankAiController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankAiController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankAiController)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_13_PROLOG
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_RPC_WRAPPERS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_INCLASS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_INCLASS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> L03_TANKMICROGAME_API UClass* StaticClass<class ATankAiController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankAiController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
