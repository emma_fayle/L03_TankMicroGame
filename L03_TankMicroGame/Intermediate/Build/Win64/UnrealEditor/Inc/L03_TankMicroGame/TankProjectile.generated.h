// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef L03_TANKMICROGAME_TankProjectile_generated_h
#error "TankProjectile.generated.h already included, missing '#pragma once' in TankProjectile.h"
#endif
#define L03_TANKMICROGAME_TankProjectile_generated_h

#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_SPARSE_DATA
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnBulletHit);


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnBulletHit);


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATankProjectile(); \
	friend struct Z_Construct_UClass_ATankProjectile_Statics; \
public: \
	DECLARE_CLASS(ATankProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATankProjectile)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATankProjectile(); \
	friend struct Z_Construct_UClass_ATankProjectile_Statics; \
public: \
	DECLARE_CLASS(ATankProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/L03_TankMicroGame"), NO_API) \
	DECLARE_SERIALIZER(ATankProjectile)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankProjectile(ATankProjectile&&); \
	NO_API ATankProjectile(const ATankProjectile&); \
public:


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankProjectile(ATankProjectile&&); \
	NO_API ATankProjectile(const ATankProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATankProjectile)


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_9_PROLOG
#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_RPC_WRAPPERS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_INCLASS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_SPARSE_DATA \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_INCLASS_NO_PURE_DECLS \
	FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> L03_TANKMICROGAME_API UClass* StaticClass<class ATankProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_L03_TankMicroGame_Source_L03_TankMicroGame_TankProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
