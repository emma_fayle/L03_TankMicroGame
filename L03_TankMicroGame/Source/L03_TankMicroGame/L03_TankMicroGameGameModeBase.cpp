// Copyright Epic Games, Inc. All Rights Reserved.


#include "L03_TankMicroGameGameModeBase.h"

AL03_TankMicroGameGameModeBase::AL03_TankMicroGameGameModeBase() {

	PlayerControllerClass = ATankPlayerController::StaticClass();
	static ConstructorHelpers::FObjectFinder<UBlueprint> TankBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Tank.BP_Tank'"));

	if (TankBlueprint.Object) {
		DefaultPawnClass = (UClass*)TankBlueprint.Object->GeneratedClass;
	}
}