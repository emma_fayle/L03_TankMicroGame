// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TankPlayerController.h"
#include "L03_TankMicroGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class L03_TANKMICROGAME_API AL03_TankMicroGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
		AL03_TankMicroGameGameModeBase();

protected:

	
};
