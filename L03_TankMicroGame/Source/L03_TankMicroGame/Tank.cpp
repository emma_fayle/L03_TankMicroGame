// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//every actor contains a root component. we initialise thsi as a scene component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	TankBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tank Body"));
	TankBody->SetupAttachment(RootComponent);

	TankTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tank Turret"));
	TankTurret->SetupAttachment(TankBody);

	TankBarrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tank Barrel"));
	TankBarrel->SetupAttachment(TankTurret);

	//Set up wheels. Each is attached to the tank body
	WheelLeftOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Left One"));
	WheelLeftOne->SetupAttachment(TankBody);
	WheelLeftTwo = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Left Two"));
	WheelLeftTwo->SetupAttachment(TankBody);
	WheelLeftThree = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Left Three"));
	WheelLeftThree->SetupAttachment(TankBody);
	WheelLeftFour = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Left Four"));
	WheelLeftFour->SetupAttachment(TankBody);

	WheelRightOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Right One"));
	WheelRightOne->SetupAttachment(TankBody);
	WheelRightTwo = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Right Two"));
	WheelRightTwo->SetupAttachment(TankBody);
	WheelRightThree = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Right Three"));
	WheelRightThree->SetupAttachment(TankBody);
	WheelRightFour = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wheel Right Four"));
	WheelRightFour->SetupAttachment(TankBody);

	//Camera and Camera arm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));

	CameraArm->SetupAttachment(TankBody);
	Camera->SetupAttachment(CameraArm,USpringArmComponent::SocketName);

	//Assign SpringArm class variables
	CameraArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
	CameraArm->TargetArmLength = 400.0f;
	CameraArm->bEnableCameraLag = true;
	CameraArm->CameraLagSpeed = 3.0f;
	MovementSpeed = 250;

	//projectile class default variable
	static ConstructorHelpers::FObjectFinder<UBlueprint> ProjectileBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_TankProjectile'"));
	if (ProjectileBlueprint.Object) {
		ProjectileClass = ProjectileBlueprint.Object->GeneratedClass;
	}

}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATank::FireCannon()
{
	FVector CannonOffset = FVector(50, 0, 60);
	FVector TankPosition = GetActorLocation();
	FVector StartPosition = CannonOffset + TankPosition;

	ATankProjectile* SpawnedBullet = (ATankProjectile*)GetWorld()->SpawnActor(ProjectileClass, &StartPosition);
	SpawnedBullet->Parent = this;
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);

}

