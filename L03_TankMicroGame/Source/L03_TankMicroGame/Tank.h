// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "TankProjectile.h"
#include "Tank.generated.h"

UCLASS()
class L03_TANKMICROGAME_API ATank : public APawn
{
	GENERATED_BODY() 
		//I dont know why this is giving me an error


public:
	// Sets default values for this pawn's properties
	ATank();
	UPROPERTY(EditAnywhere) UStaticMeshComponent* TankBody;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* TankTurret;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* TankBarrel;

	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelLeftOne;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelLeftTwo;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelLeftThree;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelLeftFour;

	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelRightOne;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelRightTwo;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelRightThree;
	UPROPERTY(EditAnywhere) UStaticMeshComponent* WheelRightFour;

	UPROPERTY(EditAnywhere) USpringArmComponent* CameraArm;
	UPROPERTY(EditAnywhere) UCameraComponent* Camera;
	UPROPERTY(EditAnywhere) float MovementSpeed;
	UPROPERTY(EditAnywhere, Category = Projectile) TSubclassOf<class ATankProjectile> ProjectileClass;
	void FireCannon();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
