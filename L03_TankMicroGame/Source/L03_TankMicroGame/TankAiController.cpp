// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAiController.h"

void ATankAiController::OnPossess(APawn* InPawn)
{

	PossessedTank = Cast<ATank>(InPawn);
	if (PossessedTank) {
		StartingLocation = PossessedTank->GetActorLocation();
		GoalLocation = StartingLocation + FVector(0, 600, 0);
	}

}

void ATankAiController::Tick(float DeltaSeconds)
{

	if (PossessedTank) {
		FVector CurrentPosition = PossessedTank->GetActorLocation();
		FVector DirectionToGoal = GoalLocation - CurrentPosition;
		DirectionToGoal.Normalize();

		CurrentPosition += (DirectionToGoal * PossessedTank->MovementSpeed * DeltaSeconds);

		if (FVector::Distance(CurrentPosition, GoalLocation) <= 1) {
			CurrentPosition = GoalLocation;
			GoalLocation = StartingLocation;
			StartingLocation = CurrentPosition;
		}
		PossessedTank->SetActorLocation(CurrentPosition);
	}

}
