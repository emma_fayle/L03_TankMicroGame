// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Tank.h"
#include "TankAiController.generated.h"

/**
 * 
 */
UCLASS()
class L03_TANKMICROGAME_API ATankAiController : public AAIController
{
	GENERATED_BODY()
	
public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere) ATank* PossessedTank;
	UPROPERTY(EditAnywhere) FVector StartingLocation;
	UPROPERTY(EditAnywhere) FVector GoalLocation;


protected:


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
