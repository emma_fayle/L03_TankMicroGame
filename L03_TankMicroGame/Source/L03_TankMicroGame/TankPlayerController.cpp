// Fill out your copyright notice in the Description page of Project Settings.


#include "TankPlayerController.h"

void ATankPlayerController::SetupInputComponent()
{

	Super::SetupInputComponent();
	InputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &ATankPlayerController::Shoot);
	InputComponent->BindAxis("MoveForward", this, &ATankPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATankPlayerController::MoveRight);

}

void ATankPlayerController::OnPossess(APawn* InPawn)
{

	Super::OnPossess(InPawn);
	PossessedTank = Cast<ATank>(InPawn);

}

void ATankPlayerController::Tick(float DeltaSeconds)
{

	Super::Tick(DeltaSeconds);
	if (PossessedTank) {
		if (!MovementInput.IsZero()) {
			MovementInput.Normalize();

			FVector CurrentLocation = PossessedTank->GetActorLocation();
			FVector Movement = (MovementInput * PossessedTank->MovementSpeed * DeltaSeconds);
			FVector NewLocation = CurrentLocation + Movement;

			PossessedTank->SetActorLocation(NewLocation); 
		}
	}

}

void ATankPlayerController::Shoot()
{
	if (PossessedTank) {
		PossessedTank->FireCannon();
	}
}

void ATankPlayerController::MoveForward(float AxisValue)
{

	MovementInput.X = AxisValue;

}

void ATankPlayerController::MoveRight(float AxisValue)
{

	MovementInput.Y = AxisValue;

}
