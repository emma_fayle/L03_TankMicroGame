// Fill out your copyright notice in the Description page of Project Settings.


#include "TankProjectile.h"
#include "Tank.h"

// Sets default values
ATankProjectile::ATankProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletMesh->OnComponentBeginOverlap.AddDynamic(this, &ATankProjectile::OnBulletHit);
	RootComponent = BulletMesh;

	Parent = nullptr;
	MovementSpeed = 950;
	MaximumLifetime = 5;
	CurrentLifetime = 0;

}

void ATankProjectile::OnBulletHit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this) {
		if (!Parent)
			return;
		if (Parent == OtherActor)
			return;
		Destroy();
		if (Cast<ATank>(OtherActor)) {
			OtherActor->Destroy();
		}
	}
}

// Called when the game starts or when spawned
void ATankProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATankProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CurrentLocation = GetActorLocation();
	FVector Forward = FVector(1, 0, 0);
	FVector Movement = (Forward * MovementSpeed * DeltaTime);
	FVector NewLocation = CurrentLocation + Movement;

	SetActorLocation(NewLocation);

	CurrentLifetime += DeltaTime;
	if (CurrentLifetime >= MaximumLifetime) {
		Destroy();
	}

}




